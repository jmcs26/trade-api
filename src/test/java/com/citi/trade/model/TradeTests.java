package com.citi.trade.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class TradeTests {

    @Test
    public void test_dateSetterAndGetter(){
        LocalDate dateCreated = LocalDate.now();
        LocalDate expectedDateCreated = dateCreated;
        Trade trade = new Trade();
        trade.setDateCreated(dateCreated);
        LocalDate actualDateCreated = trade.getDateCreated(); 
        assertEquals(expectedDateCreated, actualDateCreated);
    }

    @Test
    public void test_tickerSetterAndGetter(){
        String ticker = "APPL";
        String expectedTicker = ticker;
        Trade trade = new Trade();
        trade.setTicker(ticker);
        String actualTicker = trade.getTicker();
        assertEquals(expectedTicker, actualTicker);
    }

    @Test
    public void test_quantitySetterAndGetter(){
        int quantity = 100;
        int expectedQuantity = quantity;
        Trade trade = new Trade();
        trade.setQuantity(quantity);
        int actualQuantity = trade.getQuantity();
        assertEquals(expectedQuantity, actualQuantity);
    }

    @Test
    public void test_priceSetterAndGetter(){
        double price = 99.99;
        double expectedPrice = price;
        Trade trade = new Trade();
        trade.setPrice(price);
        double actualPrice = trade.getPrice();
        assertEquals(expectedPrice, actualPrice);
    }

    @Test
    public void test_tradeTypeSetterAndGetter(){
        TradeType tradeType = TradeType.SELL;
        TradeType expectedTradeType = tradeType;
        Trade trade = new Trade();
        trade.setTradeType(tradeType);
        TradeType actualTradeType = trade.getTradeType();
        assertEquals(expectedTradeType, actualTradeType);
    }

    @Test
    public void test_tradeStateSetterAndGetter(){
        TradeState tradeState = TradeState.PROCESSING;
        TradeState expectedTradeState = tradeState;
        Trade trade = new Trade();
        trade.setTradeState(tradeState);
        TradeState actualTradeState = trade.getTradeState();
        assertEquals(expectedTradeState, actualTradeState);
    }

    @Test
    public void test_idSetterAndGetter(){
        String id = "Test1";
        String expectedId = id;
        Trade trade = new Trade();
        trade.setId(id);
        String actualId = trade.getId();
        assertEquals(expectedId, actualId);
    }

    @Test
    public void test_totalPriceSetterAndGetter(){
        double price = 10;
        int quantity = 10;
        double expectedTotalPrice = price*quantity;
        
        Trade trade = new Trade();
        trade.setTotalPrice(price, quantity);
        double actualTotalPrice = trade.getTotalPrice();
        assertEquals(expectedTotalPrice, actualTotalPrice);
    }

    @Test
    public void test_tradeConstructor(){

        String id = "TestId";
        LocalDate dateCreated = LocalDate.now();
        String ticker = "GOOGL";
        int quantity = 12;
        double price = 225.5;
        TradeType tradeType = TradeType.BUY;
        TradeState tradeState = TradeState.CREATED;
        double totalPrice = price*quantity;
        
        Trade testTrade = new Trade(id, ticker, quantity, price, tradeType);

        String actualId = testTrade.getId();
        LocalDate actualDate = testTrade.getDateCreated();
        String actualTicker = testTrade.getTicker();
        int actualQuantity = testTrade.getQuantity();
        double actualPrice = testTrade.getPrice();
        TradeType actualTradeType = testTrade.getTradeType();
        TradeState actualTradeState = testTrade.getTradeState();
        double actualTotalPrice = testTrade.getTotalPrice();

        assertEquals(id, actualId);
        assertEquals(dateCreated, actualDate);
        assertEquals(ticker, actualTicker);
        assertEquals(quantity, actualQuantity);
        assertEquals(price, actualPrice);
        assertEquals(tradeType, actualTradeType);
        assertEquals(tradeState, actualTradeState);
        assertEquals(totalPrice, actualTotalPrice);

    }

}
