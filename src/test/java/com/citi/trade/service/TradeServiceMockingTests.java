package com.citi.trade.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import com.citi.trade.dao.TradeRepo;
import com.citi.trade.model.Trade;
import com.citi.trade.model.TradeState;
import com.citi.trade.model.TradeType;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

@WebMvcTest(TradeService.class)
public class TradeServiceMockingTests {

    @Autowired
    private TradeService tradeService;

    @MockBean
    private TradeRepo mockTradeRepo;

    @Test
    public void test_tradeService_save(){
        String id = "TestId";
        LocalDate dateCreated = LocalDate.now();
        String ticker = "GOOGL";
        int quantity = 12;
        double price = 225.5;
        TradeType tradeType = TradeType.BUY;
        TradeState tradeState = TradeState.CREATED;
        
        Trade testTrade = new Trade();
        testTrade.setId(id);
        testTrade.setDateCreated(dateCreated);
        testTrade.setTicker(ticker);
        testTrade.setQuantity(quantity);
        testTrade.setPrice(price);
        testTrade.setTradeType(tradeType);
        testTrade.setTradeState(tradeState);

        // Tell mockRepo that employeeService is going to call save()
        // when it does return the testEmployee object
        when(mockTradeRepo.save(testTrade)).thenReturn(testTrade);

        tradeService.save(testTrade);

        verify(mockTradeRepo, times(1)).save(testTrade); 

    }

}
