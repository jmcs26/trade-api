package com.citi.trade.rest;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import com.citi.trade.model.Trade;
import com.citi.trade.model.TradeState;
import com.citi.trade.model.TradeType;
import com.citi.trade.service.TradeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.mongodb.client.result.DeleteResult;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(TradeController.class)
public class TradeControllerMockingTests {

    private static final Logger logger = LoggerFactory.getLogger(TradeControllerMockingTests.class);

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeService tradeService;

    @Test
    public void test_invalidUrlReturnNotFound() throws Exception {
        this.mockMvc.perform(get("/v1/non_existing")).andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void test_getAllTrades_returnsFromService() throws Exception {

        Trade testTrade = new Trade();
        String testId = "Test 1";
        testTrade.setId(testId);
        testTrade.setDateCreated(LocalDate.now());
        testTrade.setTicker("testTicker");
        testTrade.setQuantity(5);
        testTrade.setPrice(10.5);
        testTrade.setTradeType(TradeType.BUY);
        testTrade.setTradeState(TradeState.CREATED);

        List<Trade> trades = Arrays.asList(testTrade);

        when(tradeService.findAll()).thenReturn(trades);

        this.mockMvc.perform(get("/v1/trade")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("\"id\":\"" + testId + "\"")));
    }

    @Test
    public void test_findTradeById_returnsOK() throws Exception {
        String id = "Test2";
        Trade testTrade = new Trade(id, "APPL", 10, 355.5, TradeType.BUY);

        when(tradeService.findTradeById(testTrade.getId())).thenReturn(testTrade);

        this.mockMvc.perform(get("/v1/trade/%7Bid%7D?id=" + id)).andExpect(status().isOk())
                .andExpect(content().string(containsString("\"id\":\"" + id + "\"")));

    }

    @Test
	public void test_addTrade_callsService() throws Exception {
        
        Trade testTrade = new Trade("TestId", "testTicker", 10, 355.5, TradeType.BUY);

        testTrade.setDateCreated(null);

		ObjectMapper mapper = new ObjectMapper();
    	ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
		String requestJson=ow.writeValueAsString(testTrade);
		
		System.out.println(requestJson);

		this.mockMvc.perform(post("/v1/trade").header("Content-Type", "application/json").content(requestJson))
					.andDo(print()).andExpect(status().isCreated());

		verify(tradeService, times(1)).save(any(Trade.class));
    }

    @Test
	public void test_deleteTrade_returnsNoContent() throws Exception {
        String id = "Test3";
        Trade testTrade = new Trade(id, "APPL", 10, 355.5, TradeType.BUY);

        tradeService.delete(testTrade.getId());

        this.mockMvc.perform(MockMvcRequestBuilders
                .delete("/v1/trade/" + id))
                .andExpect(status().isNoContent());
    }

}
