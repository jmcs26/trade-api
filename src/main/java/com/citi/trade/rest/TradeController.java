package com.citi.trade.rest;

import java.util.List;

import com.citi.trade.model.Trade;
import com.citi.trade.service.TradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/v1/trade")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired 
    private TradeService tradeService;

    @RequestMapping(method=RequestMethod.GET)
    public List<Trade> findAll(){

        LOG.debug("FindAll() request");

        return tradeService.findAll();
        
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public Trade findTradeById(@RequestParam("id") String id) {

        return tradeService.findTradeById(id);
    }

    @RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Trade> save (@RequestBody Trade trade){
        LOG.debug("Save() request" + trade);

        return new ResponseEntity<Trade>(tradeService.save(trade), HttpStatus.CREATED);
        
    }

    @RequestMapping(path="/{id}",method=RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable String id){
        LOG.debug("Delete() request ");
        tradeService.delete(id);

        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);

    }

    @RequestMapping(path="/{id}",method=RequestMethod.PUT)
    public ResponseEntity<Trade> update(@PathVariable String id, @RequestBody Trade trade){
        LOG.debug("update employee request received" + id);

        return new ResponseEntity<Trade>(tradeService.update(id, trade),
                                                HttpStatus.OK);

    }
    
}