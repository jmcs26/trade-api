package com.citi.trade.model;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;



@Document
public class Trade {

    @Id
    private String id;
    private LocalDate dateCreated = LocalDate.now();
    private String ticker;
    private int quantity;
    private double price;
    private TradeType tradeType;
    private TradeState tradeState = TradeState.CREATED;
    private double totalPrice;

    public Trade() {
    }

    public Trade(String id, String ticker, int quantity, double price, TradeType tradeType) {
        this.id = id;
        this.dateCreated = LocalDate.now();
        this.ticker = ticker;
        this.quantity = quantity;
        this.price = price;
        this.tradeType = tradeType;
        this.tradeState = TradeState.CREATED;
        this.totalPrice = this.price*this.quantity;
    }

    public LocalDate getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(LocalDate dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getTicker() {
        return ticker;
    }

    public void setTicker(String ticker) {
        this.ticker = ticker;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public TradeType getTradeType() {
        return tradeType;
    }

    public void setTradeType(TradeType tradeType) {
        this.tradeType = tradeType;
    }

    public TradeState getTradeState() {
        return tradeState;
    }

    public void setTradeState(TradeState tradeState) {
        this.tradeState = tradeState;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTotalPrice(double price, int quantity){
        this.totalPrice = price*quantity;
    }

    public double getTotalPrice(){
        return this.totalPrice;
    }

    @Override
    public String toString() {
        return "Trade [ID = " + id + "date created =" + dateCreated + ", price =" + price + ", quantity =" + quantity + ", ticker=" + ticker
                + ", trade state=" + tradeState + ", trade type=" + tradeType + "]";
    }

}