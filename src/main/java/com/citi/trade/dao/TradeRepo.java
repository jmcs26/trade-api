package com.citi.trade.dao;

import java.util.List;

import com.citi.trade.model.Trade;

public interface TradeRepo{

    List<Trade> findAll();

    Trade save(Trade trade);

    void deleteById(String id);

	Trade findById(String id);

}