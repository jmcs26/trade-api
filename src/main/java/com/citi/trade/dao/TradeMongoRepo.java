package com.citi.trade.dao;

import com.citi.trade.model.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface TradeMongoRepo extends TradeRepo, MongoRepository<Trade, ObjectId>{

}