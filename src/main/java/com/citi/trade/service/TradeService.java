package com.citi.trade.service;

import java.util.List;

import com.citi.trade.dao.TradeRepo;
import com.citi.trade.model.Trade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TradeService {

    @Autowired
    private  TradeRepo mongoRepo;

    public List<Trade> findAll(){

        return mongoRepo.findAll();
    }

    public Trade findTradeById(String id) {

        return mongoRepo.findById(id);

    }

    public Trade save(Trade trade) {

        return mongoRepo.save(trade);
    }

    public Trade update(String id, Trade trade){
        trade.setId(id);

        return mongoRepo.save(trade);
    }

    public void delete(String id) {

        mongoRepo.deleteById(id);
    }

}

    

